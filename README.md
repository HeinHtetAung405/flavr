# Flavr 

Flavr is a simple recepies collection application where users can surf for recepies of theirfavorite dishes.
Users can also save their favorite recepies to view them collectivelyand ready anytime.Feel the taste of
your favorite dishes with Flavr.

## Learnings

Creating this app and learning Flutter in general felt like a gift for developers.
It significantly increased development.
- Login and Register (Connect with Laravel Api)
- Pagination
- Shared_preferences
- Many Widget
- Flutter_web_browser
- Sqflite
- Progress_dialog
- Path_provider
- Http