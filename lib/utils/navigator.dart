import 'package:flavr/widgets/auth/login_screen.dart';
import 'package:flavr/widgets/auth/register_screen.dart';
import 'package:flavr/widgets/home_screen.dart';
import 'package:flavr/widgets/intro/welcome_screen.dart';
import 'package:flavr/widgets/detail/category_detail_screen.dart';
import 'package:flavr/widgets/detail/about_app_screen.dart';
import 'package:flutter/material.dart';
import 'package:flavr/model/category.dart';

goToLoginScreen(BuildContext context) {
  _pushReplaceWidgetWithFade(context, LoginScreen());
}

goToRegisterScreen(BuildContext context) {
  _pushReplaceWidgetWithFade(context, RegisterScreen());
}

goToWelcomeScreen(BuildContext context) {
  _pushReplaceWidgetWithFade(context, WelcomeScreen());
}

goToHomeScreen(BuildContext context) {
  _pushReplaceWidgetWithFade(context, HomeScreen());
}

goToCategoryDetailScreen(BuildContext context, Category category) {
  _pushWidgetWithFade(context, CategoryDetailScreen(category));
}

goToAboutAppScreen(BuildContext context) {
  _pushWidgetWithFade(context, AboutAppScreen());
}

_pushWidgetWithFade(BuildContext context, Widget widget) {
  Navigator.of(context).push(PageRouteBuilder(
    transitionsBuilder: (context, animation, secondaryAnimation, child) =>
        FadeTransition(opacity: animation, child: child),
    pageBuilder: (BuildContext context, Animation animation,
        Animation secondaryAnimation) {
      return widget;
    },
  ));
}

_pushReplaceWidgetWithFade(BuildContext context, Widget widget) {
  Navigator.of(context).pushReplacement(PageRouteBuilder(
    transitionsBuilder: (context, animation, secondaryAnimation, child) =>
        FadeTransition(opacity: animation, child: child),
    pageBuilder: (BuildContext context, Animation animation,
        Animation secondaryAnimation) {
      return widget;
    },
  ));
}
