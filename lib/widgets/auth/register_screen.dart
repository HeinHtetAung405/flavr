import 'dart:convert';

import 'package:flavr/model/user.dart';
import 'package:flavr/utils/colors.dart';
import 'package:flavr/utils/navigator.dart';
import 'package:flavr/utils/styles.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  User _user;
  TextEditingController _nameTextController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();
  ProgressDialog progressDialog;

  _showSnackBar(String content, {bool error = false}) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      backgroundColor: colorBlack,
      content: Text(
        '${error ? "An unexpecte error occurred : " : " "} $content',
        style: TextStyle(color: colorWhite),
      ),
    ));
  }

  _setValue(String key, String value) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setString(key, value);
  }

  TextFormField _createNameWidget() {
    return TextFormField(
      validator: validateName,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        labelText: 'name',
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: colorWhite)),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: colorWhite),
        ),
        labelStyle: TextStyle(color: colorWhite, fontSize: 16.0),
      ),
      style: TextStyle(color: colorWhite),
      controller: _nameTextController,
      autofocus: true,
    );
  }

  String validateName(String value) {
    if (value.isEmpty)
      return 'Please enter your name';
    else
      return null;
  }

  TextFormField _createEmailWidget() {
    return TextFormField(
      validator: validateEmail,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'email',
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: colorWhite)),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: colorWhite),
        ),
        labelStyle: TextStyle(color: colorWhite, fontSize: 16.0),
      ),
      style: TextStyle(color: colorWhite),
      controller: _emailTextController,
    );
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value)) {
      return 'Enter Valid Email';
    } else {
      return null;
    }
  }

  TextFormField _createPasswordWidget() {
    return TextFormField(
      obscureText: true,
      validator: validatePassword,
      decoration: InputDecoration(
        labelText: 'password',
        enabledBorder:
            UnderlineInputBorder(borderSide: BorderSide(color: colorWhite)),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: colorWhite),
        ),
        labelStyle: TextStyle(color: colorWhite, fontSize: 16.0),
      ),
      style: TextStyle(color: colorWhite),
      controller: _passwordTextController,
    );
  }

  String validatePassword(String value) {
    if (value.length < 6)
      return 'Password must be more than 6 charater';
    else
      return null;
  }

  Future<User> registerUser({Map body}) async {
    print(body);
    return http
        .post('https://devshub.co/api/v1/register', body: body)
        .timeout(Duration(seconds: 60))
        .then((http.Response response) {
      final int statusCode = response.statusCode;

      if (statusCode < 200 || statusCode > 400 || json == null) {
        _showSnackBar('Register failed...');
        progressDialog.hide();
        throw new Exception("Error while fetching data");
      }
      return User.fromJson(json.decode(response.body));
    });
  }

  @override
  Widget build(BuildContext context) {
    progressDialog = new ProgressDialog(context, ProgressDialogType.Normal);
    progressDialog.setMessage('Loading...');

    List<Widget> _formListWidget = [
      SizedBox(
        height: 20,
      ),
      Container(
        alignment: Alignment.center,
        child: Container(
          width: MediaQuery.of(context).size.width * .9,
          height: 150.0,
          child: Image.asset('assets/images/img_register.png'),
        ),
      ),
      SizedBox(
        height: 20,
      ),
      Container(
        alignment: Alignment.centerLeft,
        child: Container(
          padding: EdgeInsets.only(left: 20),
          child: Text(
            'REGISTER',
            style: TextStyle(
                fontSize: 26, color: colorWhite, fontFamily: fontRegular),
          ),
        ),
      ),
      SizedBox(
        height: 8,
      ),
      Container(
        padding: EdgeInsets.only(top: 8, left: 20, right: 20, bottom: 8),
        child: _createNameWidget(),
      ),
      Container(
        padding: EdgeInsets.only(top: 8, left: 20, right: 20, bottom: 8),
        child: _createEmailWidget(),
      ),
      Container(
        padding: EdgeInsets.only(top: 8, left: 20, right: 20, bottom: 8),
        child: _createPasswordWidget(),
      ),
      SizedBox(
        height: 22,
      ),
      Container(
        width: MediaQuery.of(context).size.width * .93,
        height: 50,
        child: FlatButton(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(28.0)),
          onPressed: () async {
            if (_formKey.currentState.validate()) {
              progressDialog.show();
              _formKey.currentState.save();
              _user = User(
                  id: "0",
                  name: _nameTextController.text.toString().trim(),
                  email: _emailTextController.text.toString().trim(),
                  password: _passwordTextController.text.toString().trim());
              User user = await registerUser(body: _user.toMap());
              if (user != null) {
                progressDialog.hide();
                _setValue('name', user.name);
                _setValue('email', user.email);
                goToHomeScreen(context);
              } else {
                progressDialog.hide();
                print(user.toString());
              }
            }
          },
          color: colorWhite,
          child: Text(
            "Register",
            style: TextStyle(
                color: colorMain, fontSize: 16.0, fontFamily: fontRegular),
          ),
        ),
      ),
      SizedBox(
        height: 20.0,
      ),
    ];

    Form form = Form(
        key: _formKey,
        child: ListView(
          children: _formListWidget,
        ));

    Widget bodyWidget = Container(
      child: form,
    );

    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
              icon: Icon(
                Icons.navigate_before,
                size: iconSize,
              ),
              onPressed: () => goToWelcomeScreen(context)),
          backgroundColor: colorMain,
          elevation: 0,
          title: Row(
            children: <Widget>[
              Spacer(),
              GestureDetector(
                onTap: () => goToHomeScreen(context),
                child: Text(
                  'SKIP',
                  style: TextStyle(
                      fontSize: 20.0, color: colorWhite, fontFamily: fontBold),
                ),
              )
            ],
          ),
        ),
        backgroundColor: colorMain,
        body: bodyWidget);
  }
}
