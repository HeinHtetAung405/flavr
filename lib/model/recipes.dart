class Recipes {
  int id;
  int categoryId;
  String categoryName;
  String title;
  String photo;

  factory Recipes(Map jsonMap) => Recipes._internalFromJson(jsonMap);

  Recipes._internalFromJson(Map jsonMap)
      : id = jsonMap["id"],
        categoryId = jsonMap["categoryId"],
        categoryName = jsonMap["categoryName"],
        title = jsonMap["title"] ?? "",
        photo = jsonMap["photo"] ?? "";

  Map toJson() => {
        'id': id,
        'categoryId': categoryId,
        'categoryName': categoryName ?? "",
        'title': title ?? "",
        'photo': photo ?? ""
      };

  factory Recipes.fromPrefsJson(Map jsonMap) =>
      Recipes._internalFromJson(jsonMap);
}
