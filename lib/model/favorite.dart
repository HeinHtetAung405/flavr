class Favorite {
  int _id;
  int _categoryId;
  String _categoryName;
  String _title;
  String _photo;

  Favorite(
      this._id, this._categoryId, this._categoryName, this._title, this._photo);

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map["favCategoryId"] = _categoryId;
    map["favCategoryName"] = _categoryName;
    map["favTitle"] = _title;
    map["favPhoto"] = _photo;
    if (_id != null) {
      map["favId"] = _id;
    }
    return map;
  }

  Favorite.fromObject(dynamic o) {
    this._id = o["favId"];
    this._categoryId = o["favCategoryId"];
    this._categoryName = o["favCategoryName"];
    this._title = o["favTitle"];
    this._photo = o["favPhoto"];
  }

  int get categoryId => _categoryId;

  set categoryId(int value) {
    _categoryId = value;
  }

  String get categoryName => _categoryName;

  set categoryName(String value) {
    _categoryName = value;
  }

  String get title => _title;

  set title(String value) {
    _title = value;
  }

  String get photo => _photo;

  set photo(String value) {
    _photo = value;
  }

  @override
  String toString() {
    return 'Favorite{_id: $_id, _categoryId: $_categoryId, _categoryName: $_categoryName, _title: $_title, _photo: $_photo}';
  }


}
